variables:
  IMAGE_ID: 'fe60c819e5627906377fada1e974e782c86a6b46'
  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_AMD64: "${DOCKER_REGISTRY}/bst16/amd64:${IMAGE_ID}"
  DOCKER_AARCH64: "${DOCKER_REGISTRY}/bst16/arm64:${IMAGE_ID}"
  DOCKER_POWERPC64LE: "${DOCKER_REGISTRY}/bst16/ppc64le:${IMAGE_ID}"

  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  BST_CACHE_SERVER_ADDRESS: 'freedesktop-sdk-cache.codethink.co.uk'
  # mesa-git-extension is only available through Flathub beta
  RELEASE_CHANNEL: beta

stages:
- build
- prepare-publish
- publish
- finish-publish

before_script:
  - |
    export REPO_TOKEN="${FLATHUB_REPO_TOKEN_BETA}"

  - mkdir -p ~/.config
  - mkdir -p /etc/ssl/CAS

  # Private SSL keys/certs for pushing to the CAS server
  - |
    if [ -n "$BB_CAS_PUSH_CERT" ] && [ -n "$BB_CAS_PUSH_KEY" ]; then
      echo "$BB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
      echo "$BB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key
    fi

  - |
    {
      echo "projects:"
      for project in mesa-git freedesktop-sdk; do
        echo "  ${project}:"
        echo "    artifacts:"
        if [ -f /cache-certificate/server.crt ]; then
          echo "    - url: https://local-cas-server:1102"
          echo "      client-key: /etc/ssl/CAS/server.key"
          echo "      client-cert: /etc/ssl/CAS/server.crt"
          echo "      server-cert: /cache-certificate/server.crt"
          if [ "${DISABLE_CACHE_PUSH:+set}" != set ]; then
            echo "      push: true"
          fi
        fi
        if [ -n "$GITLAB_CAS_PUSH_CERT" ] && [ -n "$GITLAB_CAS_PUSH_KEY" ]; then
          echo "    - url: https://${BST_CACHE_SERVER_ADDRESS}:11002"
          echo "      client-key: /etc/ssl/CAS/server.key"
          echo "      client-cert: /etc/ssl/CAS/server.crt"
          if [ "${DISABLE_CACHE_PUSH:+set}" != set ]; then
            echo "      push: true"
          fi
        fi
      done
    } >> ~/.config/buildstream.conf

.merge_request_build:
  stage: build
  interruptible: true
  except:
  - master
  - /^release\/.*/
  script:
  -  make build ARCH=${ARCH}
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs

.builder-x86_64:
  image: $DOCKER_AMD64
  tags:
  - x86_64
  - cache_x86_64
  variables:
    ARCH: x86_64

.builder-i686:
  image: $DOCKER_AMD64
  tags:
  - x86_64
  - cache_i686
  variables:
    ARCH: i686

build_x86_64:
  extends:
  - .merge_request_build
  - .builder-x86_64

build_i686:
  extends:
  - .merge_request_build
  - .builder-i686

.protected_build:
  only:
  - master
  - /release\/.*/ 

prepare_publish:
  extends:
  - .protected_build
  - .builder-x86_64
  stage: prepare-publish
  script:
  - flat-manager-client create https://hub.flathub.org/ "${RELEASE_CHANNEL}" > publish_build.txt
  artifacts:
    paths:
    - publish_build.txt

.publish_template: &publish
  stage: publish
  variables:
    DISABLE_CACHE_PUSH: 1
  script:
  - make export-repo RELEASE_KIND=${RELEASE_CHANNEL} REPO=repo
  - flatpak build-update-repo --generate-static-deltas --prune repo
  - flat-manager-client push $(cat publish_build.txt) repo
  dependencies:
  - prepare_publish
  artifacts:
    when: always
    paths:
    - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
  - master
  - /release\/.*/

publish_x86_64:
  extends:
  - .protected_build
  - .publish_template
  - .builder-x86_64

publish_i686:
  image: $DOCKER_AMD64
  extends:
  - .protected_build
  - .publish_template
  - .builder-i686

finish_publish:
  extends:
  - .protected_build
  - .builder-x86_64
  stage: finish-publish
  script:
  - flat-manager-client commit --wait $(cat publish_build.txt)
  - flat-manager-client publish --wait $(cat publish_build.txt)
  - flat-manager-client purge $(cat publish_build.txt)
  dependencies:
  - prepare_publish

finish_publish_failed:
  extends:
  - .protected_build
  - .builder-x86_64
  stage: finish-publish
  script:
  - flat-manager-client purge $(cat publish_build.txt)
  when: on_failure
  dependencies:
  - prepare_publish
